export interface Sensor{
  alias: string;
  id: number;
  ip: string;
  serial: string
  name: string
}
