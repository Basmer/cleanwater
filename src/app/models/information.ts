export interface Information {
  id:            number;
  registerAt:    Date;
  ph:            number;
  temperature:   number;
  contamination: number;
}
