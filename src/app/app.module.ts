import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MenuComponent } from './Components/menu/menu.component';
import { GraficaComponent } from './Components/grafica/grafica.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { HistorialComponent } from './Components/historial/historial.component';
import {HttpClientModule} from "@angular/common/http";
import { SensorListComponent } from './Components/sensor-list/sensor-list.component';
import {RouterOutlet} from "@angular/router";
import {AppRoutingModule} from "./app-routing.module";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    GraficaComponent,
    HistorialComponent,
    SensorListComponent,

  ],
  imports: [
    BrowserModule,
    NgApexchartsModule,
    HttpClientModule,
    RouterOutlet,
    AppRoutingModule,
    NgxDatatableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
