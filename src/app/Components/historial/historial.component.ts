import { Component, OnInit } from '@angular/core';
import { Registro } from './registro.interface';
import {SensorService} from "../../services/sensor.service";
import {Information} from "../../models/information"; // Ajusta la ruta a la ubicación correcta


@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})


export class HistorialComponent implements OnInit{


  constructor(private sensorService: SensorService){

  }

  private getInfomationAverage() {
    this.sensorService.getInformationSensor(1).subscribe({
      next: (response) => {
        this.registros = response;
      }
    })
  }



  public registros: Information[]  = [];

  ngOnInit(): void {
    this.getInfomationAverage();
  }




}
