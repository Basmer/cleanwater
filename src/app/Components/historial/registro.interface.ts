export interface Registro {
    id: number;
    registerAt: string;
    ph: number;
    temperature: number;
    tcd: number;
}