import {Component, OnInit, PipeTransform} from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexStroke,
  ApexDataLabels,
  ApexYAxis,
  ApexTitleSubtitle,
  ApexLegend,
  ApexOptions
} from 'ng-apexcharts';
import {SensorService} from "../../services/sensor.service";
import {Information} from "../../models/information";
import {Constants} from "../../util/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom} from "rxjs";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {DatePipe, DecimalPipe} from "@angular/common";
import {DateOnlyPipe} from "../../util/date-only.pipe";
import {TableColumn} from "@swimlane/ngx-datatable/lib/types/table-column.type";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  options?: ApexOptions,
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  yaxis: ApexYAxis;
  title: ApexTitleSubtitle;
  labels: string[];
  legend: ApexLegend;
  subtitle: ApexTitleSubtitle;
};

@Component({
  selector: 'app-grafica',
  templateUrl: './grafica.component.html',
  styleUrls: ['./grafica.component.css']
})




export class GraficaComponent implements OnInit {

  public ColumnMode = ColumnMode;

  public chartOptions!: ChartOptions;
  public chartTemperatureOptions!: ChartOptions;
  public chartContamination!: ChartOptions;
  public sensorInformationDetail: Information[] = [];

  public readonly pipeDecimalFormat: PipeTransform = {
    transform(value: any, ...args): any {
      return parseFloat(value).toFixed(2);
    }
  }


  public columns: TableColumn[] = [
    { name: 'Temperatura', prop: 'temperature', pipe: this.pipeDecimalFormat},
    { name: 'PH', prop: 'ph', pipe: this.pipeDecimalFormat},
    { name: 'Contaminacion', prop: 'contamination', pipe: this.pipeDecimalFormat},
    { name: 'Fecha', prop: 'registerAt', pipe: new DateOnlyPipe('en-US')},
    ]



  constructor(private sensorService: SensorService,
              private router: Router,

              private route: ActivatedRoute){

  }

  private async getInfomationAverage() {
    const paramsUrl = this.route.snapshot.paramMap;
    const idSensor = parseInt(paramsUrl.get('id') || '0', 10);

    if(idSensor === 0) {
      alert('Se produjo un error al buscar el sensor');
      this.router.navigate(['/']);
    }
    this.sensorService.getInformationSensorDetail(idSensor).subscribe({
      next: (response) => {
        this.sensorInformationDetail = response;
      }
    });

    this.sensorService.getInformationSensor(idSensor).subscribe({
      next: (response) => {
        this.showInformationPh(response);
        this.showInformationTemperature(response);
        this.showInformationContamination(response);
      }
    })
  }


  private showInformationTemperature(information: Information[]) {
    const series = {
      temperatura: information.map(item => parseFloat(item.temperature.toFixed(2))),
      dates: information.map(item => new Date(item.registerAt).toLocaleDateString("en-US", {
        year: 'numeric',
        month: 'short',
        day: 'numeric'
      }))
    };

    this.chartTemperatureOptions = {
      series: [{name: "TEMPERATURA", data: series.temperatura}],
      chart: {
        type: "area",
        height: 500,
        zoom: {
          enabled: true
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "straight"
      },

      title: {
        text: "Monitoreo de temperatura ",
        align: "left"
      },
      subtitle: {
        text: "Estanque",
        align: "left"
      },
      labels: series.dates,
      xaxis: {
        type: "datetime"
      },
      yaxis: {
        opposite: true
      },
      legend: {
        horizontalAlign: "left"
      }
    }
  }

  public showInformationContamination(information: Information[]) {
    const series = {
      contaminacion: information.map(item => parseFloat(item.contamination.toFixed(2))),
      dates: information.map(item => new Date(item.registerAt).toLocaleDateString("en-US", {
        year: 'numeric',
        month: 'short',
        day: 'numeric'
      }))
    };
    this.chartContamination = {
      series: [{name: "CONTAMINACION", data: series.contaminacion}],
      chart: {
        type: "area",
        height: 500,
        zoom: {
          enabled: true
        }
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "straight"
      },

      title: {
        text: "Monitoreo de Contaminacion",
        align: "left"
      },
      subtitle: {
        text: "Estanque",
        align: "left"
      },
      labels: series.dates,
      xaxis: {
        type: "datetime"
      },
      yaxis: {
        opposite: true
      },
      legend: {
        horizontalAlign: "left"
      }
    }
  }

  private showInformationPh(information: Information[]) {
    const series = {
      ph: information.map(item => parseFloat(item.ph.toFixed(2))),
      dates: information.map(item => new Date(item.registerAt).toLocaleDateString("en-US", { year: 'numeric', month: 'short', day: 'numeric' }))
    };
    this.chartOptions = {
      series: [
        {
          name: "PH",
          data: series.ph
        }
      ],
      chart: {
        type: "area",
        height: 500,
        zoom: {
          enabled: true
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "straight"
      },

      title: {
        text: "Monitoreo de PH",
        align: "left"
      },
      subtitle: {
        text: "Estanque",
        align: "left"
      },
      labels: series.dates,
      xaxis: {
        type: "datetime"
      },
      yaxis: {
        opposite: true
      },
      legend: {
        horizontalAlign: "left"
      }
    };
  }

  ngOnInit(){
    this.getInfomationAverage();
  }


}
