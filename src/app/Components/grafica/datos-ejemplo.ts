const inputData = [
  {
    "id": 1,
    "registerAt": "2023-08-09T20:11:15.838217",
    "ph": 15.77,
    "temperature": 17.0
  },
  {
    "id": 2,
    "registerAt": "2023-08-09T20:11:19.010007",
    "ph": 15.76,
    "temperature": 17.0
  }
];

const series = {
  prices: inputData.map(item => item.ph),
  temperatura: inputData.map(item => item.temperature),
  dates: inputData.map(item => new Date(item.registerAt).toLocaleDateString("en-US", { year: 'numeric', month: 'short', day: 'numeric' }))
};

/*
export const series = {
    
    prices: [
      15.77,
      15.76,
      15.80,
      
    ],
    temperatura: [
      15.10,
      18.76,
      20.80,
      
    ],
    dates: [
      "13 Nov 2017",
      "14 Nov 2017",
      "15 Nov 2017",

    ]
    
   
  };*/
  

  