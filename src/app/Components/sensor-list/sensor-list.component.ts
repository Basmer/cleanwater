import {Component, OnInit} from '@angular/core';
import {SensorService} from "../../services/sensor.service";
import {Sensor} from "../../models/sensor";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sensor-list',
  templateUrl: './sensor-list.component.html',
  styleUrls: []
})
export class SensorListComponent implements OnInit{
  public ColumnMode = ColumnMode;

  public listSensor: Sensor[] = [];
  public dtSensorOptions = {
    rows: [],
    columns: [{ name: 'Alias', prop: 'alias' },
      { name: 'Board', prop: 'name' },
      { name: 'Direccion IP', prop: 'ip' },
      {name: 'Num. Serial', prop: 'serial'}
    ]
  };

  constructor(private sensorService: SensorService, private router: Router) {
    this.initSensorDt();
  }

  private initSensorDt() {
  }


  ngOnInit(): void {
    this.consultSensors();
  }

  private consultSensors() {
    this.sensorService.getSensors().subscribe({
      next: (response) => {
        this.listSensor = response;
      }
    })
  }

  onClickSensorItem($event: any) {
    if ($event['type'] === 'click') {
      const data = $event['row'] as Sensor;
      this.router.navigate([`sensor/${data.id}`]);
    }
  }
}
