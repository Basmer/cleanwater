import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Information} from "../models/information";
import {Injectable} from "@angular/core";
import {Sensor} from "../models/sensor";


@Injectable({
  providedIn: 'root'
})
export class SensorService {

  constructor(private http: HttpClient) {
  }

  public getSensors(): Observable<Sensor[]> {
    return this.http.get<Sensor[]>('/api/sensors');
  }

  public getInformationSensor(idSensor: number): Observable<Information[]> {
    return this.http.get<Information[]>(`/api/sensors/${idSensor}/information-avg`)
  }
  public getInformationSensorDetail(idSensor: number): Observable<Information[]> {
    return this.http.get<Information[]>(`/api/sensors/${idSensor}/information`)
  }

}
