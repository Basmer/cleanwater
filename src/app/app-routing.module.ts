import {RouterModule, Routes} from "@angular/router";
import {MenuComponent} from "./Components/menu/menu.component";
import {GraficaComponent} from "./Components/grafica/grafica.component";
import {NgModule} from "@angular/core";
import {SensorListComponent} from "./Components/sensor-list/sensor-list.component";

const routes: Routes = [
  {
    path: '',
    component: MenuComponent,
    children: [
      {
        path: '',
        component: SensorListComponent,
      },
      {
        path: 'sensor/:id',
        component: GraficaComponent,
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
