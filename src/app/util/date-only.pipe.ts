import {DatePipe} from "@angular/common";

export class DateOnlyPipe extends DatePipe {
  public override transform(value:any): any {
    return super.transform(value, 'MM/dd/y hh:mm:ss');
  }
}
