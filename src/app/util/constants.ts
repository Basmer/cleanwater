export class Constants {
  public static readonly PRIMARY_COLOR = "#01B1E3";
  public static readonly PRIMARY_COLOR_DARK = "#009ACD";
  public static readonly SECONDARY_COLOR = "#00B0FF";
  public static readonly SECONDARY_COLOR_LIGHT = "#0091EA";
}
